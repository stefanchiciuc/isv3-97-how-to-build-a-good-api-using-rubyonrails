# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#

Author.create(name: 'John Doe', bio: 'John Doe is a freelance web developer and a writer.')
Author.create(name: 'Jane Smith', bio: 'Jane Smith is a freelance web developer and a writer.')
Author.create(name: 'Stefan Chiciuc', bio: 'Stefan Chiciuc is a bodybuilder and a ruby dev.')

Article.create(title: 'Hello world', content: 'lorem ipso', slug: 'hello-world', author_id: 1,
               published_date: Date.today)
Article.create(title: 'Introduction to Ruby', content: 'Ruby is a dynamic, object-oriented programming language.',
               slug: 'introduction-to-ruby', author_id: 2, published_date: Date.today)
Article.create(title: 'Getting Started with Rails', content: 'Ruby on Rails is a web application framework.',
               slug: 'getting-started-with-rails', author_id: 3, published_date: Date.today)

# db/seeds.rb

# Create a few sample comments
Comment.create(content: 'This is the first comment', article_id: 1)
Comment.create(content: 'Another comment for testing', article_id: 1)
Comment.create(content: 'A third comment just to fill the table', article_id: 2)
Comment.create(content: 'This is the fourth comment', article_id: 3)
Comment.create(content: 'And this is the fifth and last comment', article_id: 3)
Comment.create(content: 'This is the sixth comment', article_id: 2)

# You can add more comments as needed
