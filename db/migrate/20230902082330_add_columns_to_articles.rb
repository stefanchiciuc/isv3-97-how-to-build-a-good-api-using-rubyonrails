# frozen_string_literal: true

class AddColumnsToArticles < ActiveRecord::Migration[7.0]
  def change
    add_column :articles, :author, :string
    add_column :articles, :published_date, :date
  end
end
