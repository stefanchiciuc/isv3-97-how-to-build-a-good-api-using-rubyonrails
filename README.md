# How to build a good API using RubyOnRails

This API is built with Ruby on Rails and is using PostgreSQL database, the app has 3 tables: `authors`, `articles` and `comments`, all of whom has CRUD endpoints.

## Getting Started
Follow these steps to set up and run the API:

1. Clone this repository to your local machine.

2. In the `.env` file put all your environment variables from PostgreSQL.

3. Run following commands to build and run the API:
    
```bash
docker-compose build
docker-compose up -d
```

4. Run the following commands to create, migrate and seed the database:

```bash
docker-compose exec app rails db:create
docker-compose exec app rails db:migrate
docker-compose exec app rails db:seed
```

5. Open Postman and copy the `ISV3-97.postman_collection.json` file into it to verify the endpoints.