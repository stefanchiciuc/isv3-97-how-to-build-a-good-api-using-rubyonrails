# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :articles
      resources :comments
      resources :authors
    end
  end
end
